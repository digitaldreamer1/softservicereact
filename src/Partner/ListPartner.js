import React from 'react';
import axios from 'axios';
import { useState,useEffect } from "react";
import {useNavigate} from "react-router-dom";




  const ListPartner = () => {
    const [partnerss, setPartners] = useState([]);
    const navigate= useNavigate ()
    const handleUpdate = async (e,id)=> {
      navigate (`/list-partners/${id}/update`)
    }
    const handleView = async (e,id)=> {
      navigate (`/list-partners/${id}/view`)
  
    }

  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      await axios.delete(`http://localhost:3001/partners/${id}`);
      fetchPartners();
    } catch (error) {
      console.error("Il y a une erreur pour la suppression du partenaire!", error);
    }
  };

  const fetchPartners = async () => {
    try {
      const response = await axios.get("http://localhost:3001/partners");
      setPartners(response.data);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des partners!",
        error
      );
    }
  };


  useEffect(() => {
   
    fetchPartners();
  }, []);



  return (
    <div className="container mx-auto mt-8">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Liste des Partenaires</h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b">Libellé</th>
            <th className="py-2 px-4 border-b">Description</th>
            <th className="py-2 px-4 border-b">Email</th>
            <th className="py-2 px-4 border-b">Image</th>
            <th className="py-2 px-4 border-b">Lien</th>
            <th className="py-2 px-4 border-b">Téléphone</th>
            <th className="py-2 px-4 border-b">Actions</th>
          </tr>
        </thead>
        <tbody>
          {partnerss.map((partner) => (
            <tr key={partner._id}>
              <td className="py-2 px-4 border-b">{partner.libelle}</td>
              <td className="py-2 px-4 border-b">{partner.description}</td>
              <td className="py-2 px-4 border-b">{partner.email}</td>
              <td className="py-2 px-4 border-b">
                <img src={partner.image} alt={partner.libelle} className="h-16 w-16 object-cover" />
              </td>
              <td className="py-2 px-4 border-b">
                <a href={partner.link} target="_blank" rel="noopener noreferrer" className="text-blue-500 underline">
                  {partner.link}
                </a>
              </td>
              <td className="py-2 px-4 border-b">{partner.phone}</td>
              <td className="py-2 px-4 border-b">
                <button
                  className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-3 rounded-lg"
                  onClick={(e) => handleDelete(e, partner._id)}
                >
                  Supprimer
                </button>
                <button
                  className="bg-green-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleUpdate(e, partner._id)}
                >
                  Modifier
                </button>
                <button
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleView(e, partner._id)}
                >
                  Details
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListPartner;
