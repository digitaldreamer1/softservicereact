import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import axios from "axios";

const ViewPartner = () => {
  const params = useParams();
  const [partner, setPartner] = useState(null);

  const fetchPartner = async (id) => {
    try {
      const response = await axios.get("http://localhost:3001/partners/" + id);
      setPartner(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Il y a une erreur pour la récupération des partenaires!", error);
    }
  };

  useEffect(() => {
    if (params && params.id) {
      fetchPartner(params.id);
    }
    console.log(params);
  }, [params]);

  return (
    <div className="container mx-auto mt-8">
      {partner ? (
        <div className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md">
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{partner.libelle}</h2>
          <img src={partner.image} alt={partner.libelle} className="w-full h-64 object-cover mb-4 rounded-lg" />
          <p className="text-gray-700 mb-4">{partner.description}</p>
          <p className="text-gray-700 mb-4">{partner.phone}</p>

        </div>
      ) : (
        <p className="text-center text-gray-500">Chargement...</p>
      )}
    </div>
  );
}

export default ViewPartner;
