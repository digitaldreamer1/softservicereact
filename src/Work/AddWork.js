import React, { useState } from 'react';
import axios from 'axios';

const AddWork = ({ fetchWorks }) => {
  const [libelle, setLibelle] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newWork = {
      libelle,
      description,
      image,
    };
    try {
      await axios.post('http://localhost:3001/works', newWork);
      setLibelle('');
      setDescription('');
      setImage('');
      fetchWorks(); 
    } catch (error) {
      console.error("Il y a une erreur lors de l'ajout du travail!", error);
    }
  };

  return (
    <form onSubmit={handleSubmit} className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Ajouter un Travail</h2>
      <div className="mb-4">
        <label htmlFor="libelle" className="block text-gray-700 font-bold mb-2">Libellé</label>
        <input
          type="text"
          id="libelle"
          value={libelle}
          onChange={(e) => setLibelle(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="description" className="block text-gray-700 font-bold mb-2">Description</label>
        <textarea
          id="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        ></textarea>
      </div>
      <div className="mb-4">
        <label htmlFor="image" className="block text-gray-700 font-bold mb-2">Image</label>
        <input
          type="text"
          id="image"
          value={image}
          onChange={(e) => setImage(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="text-center">
        <button type="submit" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Ajouter Travail</button>
      </div>
    </form>
  );
};

export default AddWork;
