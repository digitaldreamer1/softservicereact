import React from 'react';
import axios from 'axios';
import { useState,useEffect } from "react";
import {useNavigate} from "react-router-dom";


const ListWork = () => {
  const [works, setWorks] = useState([]);
    const navigate= useNavigate ()
    const handleUpdate = async (e,id)=> {
      navigate (`/list-works/${id}/update`)
    }
    const handleView = async (e,id)=> {
      navigate (`/list-works/${id}/view`)
  
    }

  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      await axios.delete(`http://localhost:3001/works/${id}`);
      fetchWorks(); // Récupère la liste mise à jour des travaux
    } catch (error) {
      console.error("Il y a une erreur lors de la suppression du travail!", error);
    }
  };


  
  const fetchWorks = async () => {
    try {
      const response = await axios.get("http://localhost:3001/works");
      setWorks(response.data);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des travaux!",
        error
      );
    }
  };

  useEffect(() => {
    
    fetchWorks();
  
  }, []);

  return (
    <div className="container mx-auto mt-8">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Liste des Travaux</h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b">Libellé</th>
            <th className="py-2 px-4 border-b">Description</th>
            <th className="py-2 px-4 border-b">Image</th>
            <th className="py-2 px-4 border-b">Actions</th>
          </tr>
        </thead>
        <tbody>
          {works.map((work) => (
            <tr key={work._id}>
              <td className="py-2 px-4 border-b">{work.libelle}</td>
              <td className="py-2 px-4 border-b">{work.description}</td>
              <td className="py-2 px-4 border-b">
                <img src={work.image} alt={work.libelle} width="100" />
              </td>
              <td className="py-2 px-4 border-b">
                <button
                  className="py-2 px-4 bg-red-500 text-white rounded-lg"
                  onClick={(e) => handleDelete(e, work._id)}
                >
                  Supprimer
                </button>
                <button
                  className="bg-green-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleUpdate(e, work._id)}
                >
                  Modifier
                </button>
                <button
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleView(e, work._id)}
                >
                  Details
                </button>
              </td>
              
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListWork;
