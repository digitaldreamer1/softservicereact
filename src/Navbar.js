import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
  const [isOpen, setIsOpen] = useState("");

  return (
    <nav className="bg-rose-600 p-4">
      <div className="container mx-auto flex justify-between items-center">
        <div className="text-white font-bold text-lg animate-bounce">SOFTSERVICES</div>
        <div className="flex space-x-4 items-center">


          <div className="relative">
            <span
              className="text-white hover:text-gray-400 cursor-pointer"
              onClick={() => setIsOpen("contact")}
            >
              Contact
            </span>

            {isOpen==="contact" && (
              <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  <Link
                    to="/add-contact"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Ajouter Contact
                  </Link>
                  <Link
                    to="/list-contact"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Liste des Contacts
                  </Link>
                </div>
              </div>
            )}
          </div>

          <div className="relative">
            <span
              className="text-white hover:text-gray-400 cursor-pointer"
              onClick={() => setIsOpen("category")}
            >
              categorie
            </span>

            {isOpen=== "category" && (
              <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  <Link
                    to="/add-category"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Ajouter categorie
                  </Link>
                  <Link
                    to="/list-category"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Liste des categorie
                  </Link>
                </div>
              </div>
            )}
          </div>


          <div className="relative">
            <span
              className="text-white hover:text-gray-400 cursor-pointer"
              onClick={() => setIsOpen("gallery")}
            >
              Gallery
            </span>

            {isOpen=== "gallery" && (
              <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  <Link
                    to="/add-gallery"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Ajouter Gallery
                  </Link>
                  <Link
                    to="/list-galllery"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Liste des Gallery
                  </Link>
                </div>
              </div>
            )}
          </div>


          <div className="relative">
            <span
              className="text-white hover:text-gray-400 cursor-pointer"
              onClick={() => setIsOpen("partner")}
            >
              partner
            </span>

            {isOpen=== "partner" && (
              <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  <Link
                    to="/add-partner"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Ajouter Partenaires
                  </Link>
                  <Link
                    to="/list-partner"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Liste des Partenaires
                  </Link>
                </div>
              </div>
            )}
          </div>


          <div className="relative">
            <span
              className="text-white hover:text-gray-400 cursor-pointer"
              onClick={() => setIsOpen("profile")}
            >
              Profile
            </span>

            {isOpen=== "profile" && (
              <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  <Link
                    to="/add-profile"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Ajouter Profile
                  </Link>
                  <Link
                    to="/list-profile"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Liste des profiles
                  </Link>
                </div>
              </div>
            )}
          </div>

          <div className="relative">
            <span
              className="text-white hover:text-gray-400 cursor-pointer"
              onClick={() => setIsOpen("quote")}
            >
              Quote
            </span>

            {isOpen=== "quote" && (
              <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  <Link
                    to="/add-quote"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Ajouter Quote
                  </Link>
                  <Link
                    to="/list-quote"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Liste des quote
                  </Link>
                </div>
              </div>
            )}
          </div>


          <div className="relative">
            <span
              className="text-white hover:text-gray-400 cursor-pointer"
              onClick={() => setIsOpen("service")}
            >
              Service
            </span>

            {isOpen=== "service" && (
              <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  <Link
                    to="/add-services"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Ajouter Service
                  </Link>
                  <Link
                    to="/list-services"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Liste des Services
                  </Link>
                </div>
              </div>
            )}
          </div>
       

          <div className="relative">
            <span
              className="text-white hover:text-gray-400 cursor-pointer"
              onClick={() => setIsOpen("utilisateur")}
            >
              Utilisateur
            </span>

            {isOpen=== "utilisateur" && (
              <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  <Link
                    to="/add-utilisateur"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Ajouter Utilisateur
                  </Link>
                  <Link
                    to="/list-utilisateur"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Liste des Utilisateurs
                  </Link>
                </div>
              </div>
            )}
          </div>
          <div className="relative">
            <span
              className="text-white hover:text-gray-400 cursor-pointer"
              onClick={() => setIsOpen("work")}
            >
              Work
            </span>

            {isOpen=== "work" && (
              <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                  <Link
                    to="/add-work"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Ajouter Work
                  </Link>
                  <Link
                    to="/list-work"
                    className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                    role="menuitem"
                    onClick={() => setIsOpen("")}
                  >
                    Liste des Work
                  </Link>
                </div>
              </div>
            )}
          </div>

        </div>
      </div>
    </nav>
  );
}

export default Navbar;
