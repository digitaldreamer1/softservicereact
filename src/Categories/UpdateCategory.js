import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

const UpdateCategory = () => {
  const params = useParams(); //PERMET DE RECUPERER UNE VARIABLE
  const [category, setCategory] = useState(null);
  const [libelle, setLibelle] = useState(""); 
  const [description, setDescription] = useState(""); 
  const [image, setImage] = useState(""); 

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newCategory = {
      libelle,
      description,
      image,
    };
    try {
      await axios.put(
        "http://localhost:3001/categories/" + category._id,
        newCategory
      );
      console.log(newCategory);
      setLibelle(""); // SUPPRIME APRES SOUMISSION
      setDescription("");
      setImage("");
    } catch (error) {
      console.error("There was an error adding the category!", error);
    }
  };

  const fetchCategory = async (id) => {
    try {
      const response = await axios.get("http://localhost:3001/categories/" + id);
      setCategory(response.data);
      console.log(response.data);
      setLibelle(response.data.libelle); // SUPPRIME APRES SOUMISSION
      setDescription(response.data.description);
      setImage(response.data.image);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des categories!",
        error
      );
    }
  };
  useEffect(() => {
    if (params && params.id) {
      fetchCategory(params.id);
    }
    console.log(params);
  }, [params]);
 
  return (
    <form
      onSubmit={handleSubmit}
      className="max-w-md mt-8 mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md"
    >
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Ajouter une Catégorie</h2>
      <div className="mb-4">
        <label htmlFor="libelle" className="block text-gray-700 font-bold mb-2">
          Libellé
        </label>
        <input
          type="text"
          id="libelle"
          value={libelle}
          onChange={(e) => setLibelle(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="description" className="block text-gray-700 font-bold mb-2">
          Description
        </label>
        <textarea
          id="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        ></textarea>
      </div>
      <div className="mb-4">
        <label htmlFor="image" className="block text-gray-700 font-bold mb-2">
          Image URL
        </label>
        <input
          type="text"
          id="image"
          value={image}
          onChange={(e) => setImage(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="text-center">
        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg"
        >
          Modifier Catégorie
        </button>
      </div>
    </form>
  );
};

export default UpdateCategory;
