import React, { useState } from "react";
import axios from "axios";

// Composant AddCategory pour ajouter une nouvelle catégorie
const AddCategory = ({ fetchCategories }) => {
  const [libelle, setLibelle] = useState(""); 
  const [description, setDescription] = useState(""); 
  const [image, setImage] = useState(""); 

  // Fonction pour gérer la soumission du formulaire
  const handleSubmit = async (e) => {
    e.preventDefault(); 
    const newCategory = {
      libelle,
      description,
      image,
    };
    try {
      // Envoie une requête POST à l'API pour ajouter la nouvelle catégorie
      await axios.post("http://localhost:3001/categories", newCategory);
      console.log(newCategory); // Affiche la nouvelle catégorie dans la console
      // Réinitialise les champs du formulaire
      setLibelle("");
      setDescription("");
      setImage("");
      fetchCategories(); // Récupère la liste mise à jour des catégories
    } catch (error) {
      // Affiche une erreur en cas d'échec de la requête POST
      console.error("Il y a une erreur pour l'ajout de la catégorie!", error);
    }
  };

  return (
    <form
      onSubmit={handleSubmit}
      className="max-w-md mt-8 mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md"
    >
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Ajouter une Catégorie</h2>
      <div className="mb-4">
        <label htmlFor="libelle" className="block text-gray-700 font-bold mb-2">
          Libellé
        </label>
        <input
          type="text"
          id="libelle"
          value={libelle}
          onChange={(e) => setLibelle(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="description" className="block text-gray-700 font-bold mb-2">
          Description
        </label>
        <textarea
          id="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        ></textarea>
      </div>
      <div className="mb-4">
        <label htmlFor="image" className="block text-gray-700 font-bold mb-2">
          Image URL
        </label>
        <input
          type="text"
          id="image"
          value={image}
          onChange={(e) => setImage(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="text-center">
        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg"
        >
          Ajouter Catégorie
        </button>
      </div>
    </form>
  );
};

export default AddCategory;
