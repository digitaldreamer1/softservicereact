import React from "react";
import axios from "axios";
import { useState,useEffect } from "react";
import {useNavigate} from "react-router-dom";




  const ListCategory = () => {
    const [categories, setCategories] = useState([]);
    const navigate= useNavigate ()
    const handleUpdate = async (e,id)=> {
      navigate (`/list-categories/${id}/update`)
    }
    const handleView = async (e,id)=> {
      navigate (`/list-categories/${id}/view`)
  
    }

  const handleDelete = async (e, id) => {
    e.preventDefault(); 
    try {
      await axios.delete(`http://localhost:3001/categories/${id}`);
      fetchCategories(); 
    } catch (error) {
      console.error("Il y a une erreur pour la suppression de la catégorie!", error);
    }
  };


  const fetchCategories = async () => {
    try {
      const response = await axios.get("http://localhost:3001/categories");
      setCategories(response.data);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des catégories!",
        error
      );
    }
  };

  useEffect(() => {
    fetchCategories();
  }, []);

  return (
    <div className="container mx-auto mt-8">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Liste des Catégories</h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b">Libellé</th>
            <th className="py-2 px-4 border-b">Description</th>
            <th className="py-2 px-4 border-b">Image</th>
            <th className="py-2 px-4 border-b">Actions</th>
          </tr>
        </thead>
        <tbody>
          {categories.map((category) => (
            <tr key={category._id}>
              <td className="py-2 px-4 border-b">{category.libelle}</td>
              <td className="py-2 px-4 border-b">{category.description}</td>
              <td className="py-2 px-4 border-b">
                <img src={category.image} alt={category.libelle} width="100" />
              </td>
              <td className="py-2 px-4 border-b">
                <button
                  className="bg-red-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleDelete(e, category._id)}
                >
                  Supprimer
                </button>
                <button
                  className="bg-green-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleUpdate(e, category._id)}
                >
                  Modifier
                </button>
                <button
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleView(e, category._id)}
                >
                  Details
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListCategory;
