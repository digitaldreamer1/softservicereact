import React from 'react';
import axios from 'axios';
import { useState,useEffect } from "react";
import {useNavigate} from "react-router-dom";





  const ListGallery = () => {
    const [galeries, setGaleries] = useState([]);
    const navigate= useNavigate ()
    const handleUpdate = async (e,id)=> {
      navigate (`/list-galeries/${id}/update`)
    }
    const handleView = async (e,id)=> {
      navigate (`/list-galeries/${id}/view`)
  
    }
  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      await axios.delete(`http://localhost:3001/galeries/${id}`);
      fetchGaleries();
    } catch (error) {
      console.error("Il y a une erreur pour la suppression de la gallerie!", error);
    }
  };

  const fetchGaleries = async () => {
    try {
      const response = await axios.get("http://localhost:3001/galeries");
      setGaleries(response.data);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des gallerie!",
        error
      );
    }
  };

  
  useEffect(() => {
    fetchGaleries();
  }, []);

  return (
    <div className="container mx-auto mt-8">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Liste de la gallerie</h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b">image</th>
            <th className="py-2 px-4 border-b">Description</th>
            <th className="py-2 px-4 border-b">Actions</th>
          </tr>
        </thead>
        <tbody>
          {galeries.map((gallery) => (
            <tr key={gallery._id}>
              <td className="py-2 px-4 border-b">{gallery.libelle}</td>
              <td className="py-2 px-4 border-b">{gallery.description}</td>
              <td className="py-2 px-4 border-b">
                <button
                  className="py-2 px-4 border-b bg-green-500"
                  onClick={(e) => handleDelete(e, gallery._id)}
                >
                  Supprimer
                </button>
                <button
                  className="bg-green-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleUpdate(e, gallery._id)}
                >
                  Modifier
                </button>
                <button
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleView(e, gallery._id)}
                >
                  Details
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListGallery;
