import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

const UpdateGallery = () => {
    const params = useParams()
    const [gallery, setGallery] = useState(null);
    const [image, setImage] = useState('');
    const [description, setDescription] = useState('');


    const handleSubmit = async (e) => {
        e.preventDefault();
        const newGallery = { image, description };
        try {
          await axios.put('http://localhost:3001/galeries/', + gallery._id, newGallery);
          setImage('');
          setDescription('');
        } catch (error) {
          console.error("Il y a une erreur pour l'ajout de la gallery!", error);
        }
      };

  const fetchGallery = async (id) => {
    try {
        const response = await axios.get("http://localhost:3001/galeries/" +id);
        setGallery(response.data);
        console.log(response.data);
        setImage(response.data.image);
        setDescription(response.data.description)
      } catch (error) {
        console.error(
          "Il y a une erreur pour la récupération des gallerie!",
          error
        );
      }
    };
  useEffect(() => {
    if (params && params.id) {
      fetchGallery(params.id);
    }
    console.log(params);
  }, [params]);


  return (
    <form onSubmit={handleSubmit} className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Ajouter une image</h2>
      <div className="mb-4">
        <label htmlFor="libelle" className="block text-gray-700 font-bold mb-2">image</label>
        <input
          type="text"
          id="image"
          value={image}
          onChange={(e) => setImage(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="description" className="block text-gray-700 font-bold mb-2">Description</label>
        <textarea
          id="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        ></textarea>
      </div>
      <div className="text-center">
        <button type="submit" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">
          Modifier Gallery
        </button>
      </div>
    </form>
  );
};

export default UpdateGallery;