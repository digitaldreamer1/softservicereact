import React from "react";
import axios from "axios";
import { useState,useEffect } from "react";
import {useNavigate} from "react-router-dom";




  const ListUtilisateur = () => {
    const [utilisateurs, setUtilisateurs] = useState([]);
    const navigate= useNavigate ()
    const handleUpdate = async (e,id)=> {
      navigate (`/list-utilisateurs/${id}/update`)
    }
    const handleView = async (e,id)=> {
      navigate (`/list-utilisateurs/${id}/view`)
  
    }

  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      await axios.delete(`http://localhost:3001/utilisateurs/${id}`);
      fetchUtilisateurs(); 
    } catch (error) {
      console.error("une erreur s'est produite !", error);
    }
  };


  const fetchUtilisateurs = async () => {
    try {
      const response = await axios.get("http://localhost:3001/utilisateurs");
      setUtilisateurs(response.data);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des utilisateurs!",
        error
      );
    }
  };


 useEffect(() => {
   
    fetchUtilisateurs();
  
  }, []);

  return (
    <div className="container mx-auto mt-8">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">
        Liste des Utilisateurs
      </h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b">Prénom</th>
            <th className="py-2 px-4 border-b">Nom de famille</th>
            <th className="py-2 px-4 border-b">Email</th>
            <th className="py-2 px-4 border-b">Téléphone</th>
            <th className="py-2 px-4 border-b">Adresse</th>
            <th className="py-2 px-4 border-b">Genre</th>
            <th className="py-2 px-4 border-b">Date de naissance</th>
            <th className="py-2 px-4 border-b">Lieu de naissance</th>
            <th className="py-2 px-4 border-b">Âge</th>
            <th className="py-2 px-4 border-b">Actions</th>
          </tr>
        </thead>
        <tbody>
          {utilisateurs.map((utilisateur) => (
            <tr key={utilisateur._id}>
              <td className="py-2 px-4 border-b">{utilisateur.firstname}</td>
              <td className="py-2 px-4 border-b">{utilisateur.lastname}</td>
              <td className="py-2 px-4 border-b">{utilisateur.email}</td>
              <td className="py-2 px-4 border-b">{utilisateur.phone}</td>
              <td className="py-2 px-4 border-b">{utilisateur.address}</td>
              <td className="py-2 px-4 border-b">{utilisateur.gender}</td>
              <td className="py-2 px-4 border-b">{utilisateur.birthday}</td>
              <td className="py-2 px-4 border-b">{utilisateur.birthplace}</td>
              <td className="py-2 px-4 border-b">{utilisateur.age}</td>
              <td className="py-2 px-4 border-b">
                <button
                  className="py-2 bg-red-500 rounded px-4 border-b"
                  onClick={(e) => handleDelete(e, utilisateur._id)}
                >
                  Supprimer
                </button>
                <button
                  className="bg-green-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleUpdate(e, utilisateur._id)}
                >
                  Modifier
                </button>
                <button
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleView(e, utilisateur._id)}
                >
                  Details
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListUtilisateur;
