import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import axios from "axios";

const ViewUtilisateur = () => {
  const params = useParams();
  const [utilisateur, setUtilisateur] = useState(null);

  const fetchUtilisateur = async (id) => {
    try {
      const response = await axios.get("http://localhost:3001/utilisateurs/" + id);
      setUtilisateur(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Il y a une erreur pour la récupération des utilisateurs!", error);
    }
  };

  useEffect(() => {
    if (params && params.id) {
      fetchUtilisateur(params.id);
    }
    console.log(params);
  }, [params]);

  return (
    <div className="container mx-auto mt-8">
      {utilisateur ? (
        <div className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md">
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{utilisateur.firstname}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{utilisateur.lastname}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{utilisateur.email}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{utilisateur.phone}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{utilisateur.address}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{utilisateur.birthday}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{utilisateur.birthplace}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{utilisateur.age}</h2>

        </div>
      ) : (
        <p className="text-center text-gray-500">Chargement...</p>
      )}
    </div>
  );
}

export default ViewUtilisateur;
