import React,{useState,useEffect} from 'react';
import {useParams} from "react-router-dom"
import axios from "axios"


const UpdateUtilisateur = () => {
const params=useParams () ;
const [utilisateur,setUtilisateur]= useState(null)

const [firstname, setFirstname] = useState("");
const [lastname, setLastname] = useState("");
const [email, setEmail] = useState("");
const [phone, setPhone] = useState("");
const [address, setAddress] = useState("");
const [gender, setGender] = useState("");
const [birthday, setBirthday] = useState("");
const [birthplace, setBirthplace] = useState("");
const [age, setAge] = useState("");
  
const handleSubmit = async (e) => {
    e.preventDefault();
    const newUtilisateur = {
      firstname,
      lastname,
      email,
      phone,
      address,
      gender,
      birthday,
      birthplace,
      age,
    };

    try {
        await axios.put("http://localhost:3001/utilisateurs" +utilisateur._id, newUtilisateur);
        console.log(newUtilisateur);
        setFirstname("");
        setLastname("");
        setEmail("");
        setPhone("");
        setAddress("");
        setGender("");
        setBirthday("");
        setBirthplace("");
        setAge("");
      } catch (error) {
        console.error("une erreur s'est produite!", error);
      }
    };
    const fetchUtilisateur = async (id) => {
        try {
          const response = await axios.get("http://localhost:3001/utilisateurs/" +id);
          setUtilisateur(response.data);
          console.log(response.data);
          setFirstname(response.data.firstname); // SUPPRIME APRES SOUMISSION
          setLastname(response.data.lastname);
          setEmail(response.data.email);
          setPhone(response.data.phone);
          setAddress(response.data.address);
          setGender(response.data.gender);
          setBirthday(response.data.birthday);
          setBirthplace(response.data.birthplace);
          setAge(response.data.age);

        } catch (error) {
          console.error(
            "Il y a une erreur pour la récupération des services!",
            error
          );
        }
      };
      useEffect(() => {
        if (params && params.id)
            { fetchUtilisateur(params.id)}
    console.log(params)
      }, [params]);
      return (
        <div>
<form
      onSubmit={handleSubmit}
      className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md"
    >
      <h2 className="text-2xl font-bold mb-6 text-gray-800">
        Ajouter un Utilisateur
      </h2>
      <div className="mb-4">
        <label htmlFor="firstname" className="block text-gray-700 font-bold mb-2">
          Prénom
        </label>
        <input
          type="text"
          id="firstname"
          value={firstname}
          onChange={(e) => setFirstname(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="lastname" className="block text-gray-700 font-bold mb-2">
          Nom de famille
        </label>
        <input
          type="text"
          id="lastname"
          value={lastname}
          onChange={(e) => setLastname(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="email" className="block text-gray-700 font-bold mb-2">
          Email
        </label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="phone" className="block text-gray-700 font-bold mb-2">
          Téléphone
        </label>
        <input
          type="text"
          id="phone"
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      
      <div className="mb-4">
        <label htmlFor="address" className="block text-gray-700 font-bold mb-2">
          Adresse
        </label>
        <input
          type="text"
          id="address"
          value={address}
          onChange={(e) => setAddress(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="gender" className="block text-gray-700 font-bold mb-2">
          Genre
        </label>
        <input
          type="text"
          id="gender"
          value={gender}
          onChange={(e) => setGender(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="birthday" className="block text-gray-700 font-bold mb-2">
          Date de naissance
        </label>
        <input
          type="text"
          id="birthday"
          value={birthday}
          onChange={(e) => setBirthday(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="birthplace" className="block text-gray-700 font-bold mb-2">
          Lieu de naissance
        </label>
        <input
          type="text"
          id="birthplace"
          value={birthplace}
          onChange={(e) => setBirthplace(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="age" className="block text-gray-700 font-bold mb-2">
          Âge
        </label>
        <input
          type="text"
          id="age"
          value={age}
          onChange={(e) => setAge(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="text-center">
        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg"
        >
          Modifier Utilisateur
        </button>
        </div>
    </form>
        </div>
    );
}

export default UpdateUtilisateur;