import Navbar from "./Navbar";
import AddService from "./Services/AddServices";
import ListServices from "./Services/ListServices";
import UpdateServices from "./Services/UpdateServices";
import ViewServices from "./Services/ViewServices";
import AddCategory from "./Categories/AddCategory";
import ListCategory from "./Categories/ListCategory";
import AddContact from "./Contact/AddContact";
import AddQuote from "./Quote/AddQuote";
import ListQuote from "./Quote/ListQuote";
import AddProfile from "./Profile/AddProfile";
import ListProfile from "./Profile/ListProfile";
import AddPartner from "./Partner/AddPartner";
import ListPartner from "./Partner/ListPartner";
import ListGalllery from "./Gallery/ListGalllery";
import ListUtilisateur from "./Utilisateur/ListUtilisateur";
import AddUtilisateur from "./Utilisateur/AddUtilisateur";
import AddWork from "./Work/AddWork";
import ListWork from "./Work/ListWork";
import ViewWorks from "./Work/ViewWork";
import { Routes, Route } from "react-router-dom";
import ListContacts from "./Contact/ListContact";
import AddGallery from "./Gallery/AddGallery";
import ViewGallery from "./Gallery/ViewGallery";
import ViewContact from "./Contact/ViewContact";
import ViewPartner from "./Partner/ViewPartner";
import ViewProfile from "./Profile/ViewProfile";
import ViewCategory from "./Categories/ViewCategory";
import ViewQuote from "./Quote/ViewQuote";
import ViewUtilisateur from "./Utilisateur/ViewUtilisateur";
import UpdateUtilisateur from "./Utilisateur/UpdateUtilisateur";
import UpdateWork from "./Work/UpdateWork";
import UpdateQuote from "./Quote/UpdateQuote";
import UpdateProfile from "./Profile/UpdateProfile";
import UpdatePartner from "./Partner/UpdatePartner";
import UpdateGallery from "./Gallery/UpdateGallery";
import UpdateContact from "./Contact/UpdateContact";
import UpdateCategory from "./Categories/UpdateCategory";



function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<ListServices />} />
        <Route path="/list-services" element={<ListServices />} />
        <Route path="/list-services/:id/update" element={<UpdateServices />}/>{/* ROUTES POUR MODIFIER*/}
        <Route path="/list-services/:id/view" element={<ViewServices />} />
        <Route path="/add-services" element={<AddService />} />

        <Route path="/list-quote" element={<ListQuote />} />
        <Route path="/add-quote" element={<AddQuote />} />
        <Route path="/list-quotes/:id/view" element={<ViewQuote />} />
        <Route path="/list-quotes/:id/update" element={<UpdateQuote />}/>{/* ROUTES POUR MODIFIER*/}


        <Route path="/list-profile" element={<ListProfile />} />
        <Route path="/list-profiles/:id/view" element={<ViewProfile />} />
        <Route path="/list-profiles/:id/update" element={<UpdateProfile />}/>{/* ROUTES POUR MODIFIER*/}
        <Route path="/add-profile" element={<AddProfile />} />

        <Route path="/list-partner" element={<ListPartner />} />
        <Route path="/list-partners/:id/view" element={<ViewPartner />} />
        <Route path="/list-partners/:id/update" element={<UpdatePartner />}/>{/* ROUTES POUR MODIFIER*/}
        <Route path="/add-partner" element={<AddPartner />} />

        <Route path="/list-galllery" element={<ListGalllery />} />
        <Route path="/add-gallery" element={<AddGallery />} />
        <Route path="/list-galeries/:id/view" element={<ViewGallery />} />
        <Route path="/list-galeries/:id/update" element={<UpdateGallery />}/>{/* ROUTES POUR MODIFIER*/}

        <Route path="/list-contact" element={<ListContacts />} />
        <Route path="/list-contacts/:id/view" element={<ViewContact />} />
        <Route path="/add-contact" element={<AddContact />} />
        <Route path="/list-contacts/:id/update" element={<UpdateContact />}/>{/* ROUTES POUR MODIFIER*/}

        <Route path="/add-category" element={<AddCategory />} />
        <Route path="/list-category" element={<ListCategory />} />
        <Route path="/list-categories/:id/view" element={<ViewCategory />} />
        <Route path="/list-categories/:id/update" element={<UpdateCategory />}/>{/* ROUTES POUR MODIFIER*/}

        <Route path="/list-utilisateur" element={<ListUtilisateur />} />
        <Route path="/add-utilisateur" element={<AddUtilisateur />} />
        <Route path="/list-utilisateurs/:id/view" element={<ViewUtilisateur />} />
        <Route path="/list-utilisateurs/:id/update" element={<UpdateUtilisateur />}/>{/* ROUTES POUR MODIFIER*/}


        <Route path="/add-work" element={<AddWork />} />
        <Route path="/list-work" element={<ListWork />} />
        <Route path="/list-works/:id/view" element={<ViewWorks />} />
        <Route path="/list-works/:id/update" element={<UpdateWork />}/>{/* ROUTES POUR MODIFIER*/}


      </Routes>
    </div>
  );
}

export default App;
