import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

const UpdateServices = () => {
  const params = useParams(); //PERMET DE RECUPERER UNE VARIABLE
  const [service, setService] = useState(null);
  const [libelle, setLibelle] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const [categoryId, setCategoryId] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newService = {
      libelle,
      description,
      image,
      category_id: categoryId,
    };
    try {
      await axios.put(
        "http://localhost:3001/services/" + service._id,
        newService
      );
      console.log(newService);
      setLibelle(""); // SUPPRIME APRES SOUMISSION
      setDescription("");
      setImage("");
      setCategoryId("");
    } catch (error) {
      console.error("There was an error adding the service!", error);
    }
  };

  const fetchService = async (id) => {
    try {
      const response = await axios.get("http://localhost:3001/services/" + id);
      setService(response.data);
      console.log(response.data);
      setLibelle(response.data.libelle); // SUPPRIME APRES SOUMISSION
      setDescription(response.data.description);
      setImage(response.data.image);
      setCategoryId(response.data.category_id);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des services!",
        error
      );
    }
  };
  useEffect(() => {
    if (params && params.id) {
      fetchService(params.id);
    }
    console.log(params);
  }, [params]);
  return (
    <div>
      <form
        onSubmit={handleSubmit}
        className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md"
      >
        <h2 className="text-2xl font-bold mb-6 text-gray-800">
          Ajouter un Service
        </h2>
        <div className="mb-4">
          <label
            htmlFor="libelle"
            className="block text-gray-700 font-bold mb-2"
          >
            Libellé
          </label>
          <input
            type="text"
            id="libelle"
            value={libelle}
            onChange={(e) => setLibelle(e.target.value)}
            className="w-full p-2 border border-gray-300 rounded-lg"
            required
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="description"
            className="block text-gray-700 font-bold mb-2"
          >
            Description
          </label>
          <textarea
            id="description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            className="w-full p-2 border border-gray-300 rounded-lg"
            required
          ></textarea>
        </div>
        <div className="mb-4">
          <label htmlFor="image" className="block text-gray-700 font-bold mb-2">
            Image URL
          </label>
          <input
            type="text"
            id="image"
            value={image}
            onChange={(e) => setImage(e.target.value)}
            className="w-full p-2 border border-gray-300 rounded-lg"
            required
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="categoryId"
            className="block text-gray-700 font-bold mb-2"
          >
            Category ID
          </label>
          <select
            onChange={(e) => setCategoryId(e.target.value)}
            id="categoryId"
            className="bg-white-500 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            required
          >
            <option value="" disabled>
              Choisir une categorie
            </option>
            <option value="662fcc5600103ce715a474c6">Category 1</option>
            <option value="662fcc6c00103ce715a474c8">Category 2</option>
            <option value="663103df352488ad50dde9be">Category 3</option>
            <option value="6631070327e0579e632d184c">Category 4</option>
          </select>
        </div>
        <div className="text-center">
          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg"
          >
            Modifier Service
          </button>
        </div>
      </form>
    </div>
  );
};

export default UpdateServices;
