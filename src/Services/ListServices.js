import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import {useNavigate} from "react-router-dom";


const ListServices = () => {
  const [services, setServices] = useState([]);
  const navigate= useNavigate ()
  const handleUpdate = async (e,id)=> {
    navigate (`/list-services/${id}/update`)
  }
  const handleView = async (e,id)=> {
    navigate (`/list-services/${id}/view`)

  }


  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      await axios.delete(`http://localhost:3001/services/${id}`);
      fetchServices(); // MISE A JOUR DES SERVICES
    } catch (error) {
      console.error("une erreur s'est produite!", error);
    }
  };

  const fetchServices = async () => {
    try {
      const response = await axios.get("http://localhost:3001/services");
      setServices(response.data);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des services!",
        error
      );
    }
  };

  useEffect(() => {
    fetchServices();
  }, []);

  return (
    <div className="container mx-auto mt-8">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">
        Liste des Services
      </h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b">Libellé</th>
            <th className="py-2 px-4 border-b">Description</th>
            <th className="py-2 px-4 border-b">Image</th>
            <th className="py-2 px-4 border-b">Category ID</th>
            <th className="py-2 px-4 border-b">Actions</th>
          </tr>
        </thead>
        <tbody>
          {services.map((service) => (
            <tr key={service._id}>
              <td className="py-2 px-4 border-b">{service.libelle}</td>
              <td className="py-2 px-4 border-b">{service.description}</td>
              <td className="py-2 px-4 border-b">
                <img src={service.image} alt={service.libelle} width="100" />
              </td>
              <td className="py-2 px-4 border-b">
                {service.category_id ? service.category_id.libelle : ""}
              </td>
              <td className="py-2 px-4 border-b">
                <button
                  className="bg-red-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleDelete(e, service._id)}
                >
                  Supprimer
                </button>
                <button
                  className="bg-green-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleUpdate(e, service._id)}
                >
                  Modifier
                </button>
                <button
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleView(e, service._id)}
                >
                  Details
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListServices;
