import React from "react";
import axios from "axios";
import { useState,useEffect } from "react";
import {useNavigate} from "react-router-dom";




  const ListContact = () => {
      // Fonction pour gérer la suppression d'un contact

    const [contacts, setContacts] = useState([]);
    const navigate= useNavigate ()
    const handleUpdate = async (e,id)=> {
      navigate (`/list-contacts/${id}/update`)
    }
    const handleView = async (e,id)=> {
      navigate (`/list-contacts/${id}/view`)
  
    }

  const handleDelete = async (e, id) => {
    e.preventDefault(); 
    try {
      await axios.delete(`http://localhost:3001/contacts/${id}`);
      fetchContacts(); // Récupère la liste mise à jour des contacts
    } catch (error) {
    
      console.error("Il y a une erreur pour la suppression du contact!", error);
    }
  };

  const fetchContacts = async () => {
    try {
      const response = await axios.get("http://localhost:3001/contacts");
      setContacts(response.data);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des contacts!",
        error
      );
    }
  };

  useEffect(() => {
    fetchContacts();
    
  }, []);



  return (
    <div className="container mx-auto mt-8">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Liste des Contacts</h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b">Prénom</th>
            <th className="py-2 px-4 border-b">Nom</th>
            <th className="py-2 px-4 border-b">Email</th>
            <th className="py-2 px-4 border-b">Téléphone</th>
            <th className="py-2 px-4 border-b">Sujet</th>
            <th className="py-2 px-4 border-b">Message</th>
            <th className="py-2 px-4 border-b">Actions</th>
          </tr>
        </thead>
        <tbody>
          {contacts.map((contact) => (
            <tr key={contact._id}>
              <td className="py-2 px-4 border-b">{contact.firstname}</td>
              <td className="py-2 px-4 border-b">{contact.lastname}</td>
              <td className="py-2 px-4 border-b">{contact.email}</td>
              <td className="py-2 px-4 border-b">{contact.phone}</td>
              <td className="py-2 px-4 border-b">{contact.subject}</td>
              <td className="py-2 px-4 border-b">{contact.message}</td>
              <td className="py-2 px-4 border-b">
                <button
                  className="bg-red-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleDelete(e, contact._id)}
                >
                  Supprimer
                </button>
                <button
                  className="bg-green-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleUpdate(e, contact._id)}
                >
                  Modifier
                </button>
                <button
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleView(e, contact._id)}
                >
                  Details
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListContact;
