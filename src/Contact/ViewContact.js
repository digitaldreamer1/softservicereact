import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import axios from "axios";

const ViewContact = () => {
  const params = useParams();
  const [contact, setContact] = useState(null);

  const fetchCategory = async (id) => {
    try {
      const response = await axios.get("http://localhost:3001/contacts/" + id);
      setContact(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Il y a une erreur pour la récupération des contacts!", error);
    }
  };

  useEffect(() => {
    if (params && params.id) {
      fetchCategory(params.id);
    }
    console.log(params);
  }, [params]);

  return (
    <div className="container mx-auto mt-8">
      {contact ? (
        <div className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md">
          <p className="text-gray-700 mb-4">{contact.firstname}</p>
          <p className="text-gray-700 mb-4">{contact.lastname}</p>
          <p className="text-gray-700 mb-4">{contact.email}</p>
          <p className="text-gray-700 mb-4">{contact.phone}</p>
          <p className="text-gray-700 mb-4">{contact.subject}</p>
          <p className="text-gray-700 mb-4">{contact.message}</p>
<h2 className="text-2xl font-bold mb-4 text-gray-800">{contact.firstname}</h2>
<h2 className="text-2xl font-bold mb-4 text-gray-800">{contact.lastname}</h2>
<h2 className="text-2xl font-bold mb-4 text-gray-800">{contact.email}</h2>
<h2 className="text-2xl font-bold mb-4 text-gray-800">{contact.phone}</h2>
<h2 className="text-2xl font-bold mb-4 text-gray-800">{contact.subject}</h2>
<h2 className="text-2xl font-bold mb-4 text-gray-800">{contact.message}</h2>
        </div>
      ) : (
        <p className="text-center text-gray-500">Chargement...</p>
      )}
    </div>
  );
}

export default ViewContact;
