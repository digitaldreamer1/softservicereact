import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";


const UpdateProfile = () => {
    const params = useParams();
const [libelle, setLibelle] = useState('');
const [profile, setProfile] = useState(null);

  const [description, setDescription] = useState('');
  const handleSubmit = async (e) => {
    e.preventDefault();
    const newProfile = { libelle, description };
    try {
        await axios.put('http://localhost:3001/profiles', +profile._id, newProfile );
        console.log(newProfile);

        setLibelle('');
        setDescription('');
      } catch (error) {
        console.error("Il y a une erreur pour l'ajout du profil!", error);
      }
    };

    const fetchProfiles = async (id) => {
        try {
          const response = await axios.get("http://localhost:3001/profiles/" + id);
          setProfile(response.data);
          setLibelle(response.data.libelle);
          setDescription(response.data.description);

        } catch (error) {
          console.error(
            "Il y a une erreur pour la récupération des profils!",
            error
          );
        }
      };
      useEffect(() => {
        if (params && params.id) {
          fetchProfiles(params.id);
        }
        console.log(params);
      }, [params]);

      return (
        <form onSubmit={handleSubmit} className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md">
          <h2 className="text-2xl font-bold mb-6 text-gray-800">Ajouter un Profil</h2>
          <div className="mb-4">
            <label htmlFor="libelle" className="block text-gray-700 font-bold mb-2">Libellé</label>
            <input
              type="text"
              id="libelle"
              value={libelle}
              onChange={(e) => setLibelle(e.target.value)}
              className="w-full p-2 border border-gray-300 rounded-lg"
              required
            />
          </div>
          <div className="mb-4">
            <label htmlFor="description" className="block text-gray-700 font-bold mb-2">Description</label>
            <textarea
              id="description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              className="w-full p-2 border border-gray-300 rounded-lg"
              required
            ></textarea>
          </div>
          <div className="text-center">
            <button type="submit" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">
              Ajouter Profil
            </button>
          </div>
        </form>
      );
    };
    
    export default UpdateProfile;
    
