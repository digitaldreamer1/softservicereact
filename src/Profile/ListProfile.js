import React from 'react';
import axios from 'axios';
import { useState,useEffect } from "react";
import {useNavigate} from "react-router-dom";




  const ListProfile = () => {
    const [profiles, setProfiles] = useState([]);
    const navigate= useNavigate ()
    const handleUpdate = async (e,id)=> {
      navigate (`/list-profiles/${id}/update`)
    }
    const handleView = async (e,id)=> {
      navigate (`/list-profiles/${id}/view`)
  
    }


  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      await axios.delete(`http://localhost:3001/profiles/${id}`);
      fetchProfiles();
    } catch (error) {
      console.error("Il y a une erreur pour la suppression du profil!", error);
    }
  };

  const fetchProfiles = async () => {
    try {
      const response = await axios.get("http://localhost:3001/profiles");
      setProfiles(response.data);
    } catch (error) {
      console.error(
        "Il y a une erreur pour la récupération des profils!",
        error
      );
    }
  };


 useEffect(() => {
   
    fetchProfiles();
   
  }, []);

  return (
    <div className="container mx-auto mt-8">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Liste des Profils</h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b">Libellé</th>
            <th className="py-2 px-4 border-b">Description</th>
            <th className="py-2 px-4 border-b">Actions</th>
          </tr>
        </thead>
        <tbody>
          {profiles.map((profile) => (
            <tr key={profile._id}>
              <td className="py-2 px-4 border-b">{profile.libelle}</td>
              <td className="py-2 px-4 border-b">{profile.description}</td>
              <td className="py-2 px-4 border-b">
                <button
                  className="py-2 px-4 border-b bg-green-500"
                  onClick={(e) => handleDelete(e, profile._id)}
                >
                  Supprimer
                </button>
                <button
                  className="bg-green-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleUpdate(e, profile._id)}
                >
                  Modifier
                </button>
                <button
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleView(e, profile._id)}
                >
                  Details
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListProfile;
