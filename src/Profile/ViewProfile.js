import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import axios from "axios";

const ViewProfile = () => {
  const params = useParams();
  const [profile, setProfile] = useState(null);

  const fetchProfile = async (id) => {
    try {
      const response = await axios.get("http://localhost:3001/profiles/" + id);
      setProfile(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Il y a une erreur pour la récupération des profils!", error);
    }
  };

  useEffect(() => {
    if (params && params.id) {
      fetchProfile(params.id);
    }
    console.log(params);
  }, [params]);

  return (
    <div className="container mx-auto mt-8">
      {profile ? (
        <div className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md">
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{profile.libelle}</h2>
          <img src={profile.image} alt={profile.libelle} className="w-full h-64 object-cover mb-4 rounded-lg" />
          <p className="text-gray-700 mb-4">{profile.description}</p>
        </div>
      ) : (
        <p className="text-center text-gray-500">Chargement...</p>
      )}
    </div>
  );
}

export default ViewProfile;
