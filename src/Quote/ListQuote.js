import React from 'react';
import axios from 'axios';
import { useState,useEffect } from "react";
import {useNavigate} from "react-router-dom";




  const ListQuote = () => {
    const [quotes, setQuotes] = useState([]);
    const navigate= useNavigate ()
    const handleUpdate = async (e,id)=> {
      navigate (`/list-quotes/${id}/update`)
    }
    const handleView = async (e,id)=> {
      navigate (`/list-quotes/${id}/view`)
  
    }

  const handleDelete = async (e, id) => {
    e.preventDefault();
    try {
      await axios.delete(`http://localhost:3001/quotes/${id}`);
      fetchQuotes(); 
    } catch (error) {
      console.error("une erreur s'est produite!", error);
    }
  };

  const fetchQuotes = async () => {
    try {
      const response = await axios.get("http://localhost:3001/quotes");
      setQuotes(response.data);
    } catch (error) {
      console.error("Il y a une erreur pour la récupération des devis!", error);
    }
  };

  useEffect(() => {
   
    fetchQuotes();

  }, []);



  return (
    <div className="container mx-auto mt-8">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Liste des Devis</h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b">Prénom</th>
            <th className="py-2 px-4 border-b">Nom</th>
            <th className="py-2 px-4 border-b">Email</th>
            <th className="py-2 px-4 border-b">Téléphone</th>
            <th className="py-2 px-4 border-b">Sujet</th>
            <th className="py-2 px-4 border-b">Message</th>
            <th className="py-2 px-4 border-b">Service ID</th>
            <th className="py-2 px-4 border-b">Actions</th>
          </tr>
        </thead>
        <tbody>
          {quotes.map((quote) => (
            <tr key={quote._id}>
              <td className="py-2 px-4 border-b">{quote.firstname}</td>
              <td className="py-2 px-4 border-b">{quote.lastname}</td>
              <td className="py-2 px-4 border-b">{quote.email}</td>
              <td className="py-2 px-4 border-b">{quote.phone}</td>
              <td className="py-2 px-4 border-b">{quote.subject}</td>
              <td className="py-2 px-4 border-b">{quote.message}</td>
              <td className="py-2 px-4 border-b">{quote.service_id}</td>
              <td className="py-2 px-4 border-b">
                <button
                  className="py-2 px-4 bg-red-500 text-white rounded-lg"
                  onClick={(e) => handleDelete(e, quote._id)}
                >
                  Supprimer
                </button>
                <button
                  className="bg-green-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleUpdate(e, quote._id)}
                >
                  Modifier
                </button>
                <button
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                  onClick={(e) => handleView(e, quote._id)}
                >
                  Details
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListQuote;
