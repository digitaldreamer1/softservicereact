import React, { useState } from 'react';
import axios from 'axios';

const AddQuote = ({ fetchQuotes }) => {
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');
  const [serviceId, setServiceId] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newQuote = {
      firstname,
      lastname,
      email,
      phone,
      subject,
      message,
      service_id: serviceId,
    };
    try {
      await axios.post('http://localhost:3001/quotes', newQuote);
      console.log(newQuote);
      setFirstname('');
      setLastname('');
      setEmail('');
      setPhone('');
      setSubject('');
      setMessage('');
      setServiceId('');
      fetchQuotes(); // Récupère la liste mise à jour des devis
    } catch (error) {
      console.error("Il y a une erreur lors de l'ajout du devis!", error);
    }
  };

  return (
    <form onSubmit={handleSubmit} className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md">
      <h2 className="text-2xl font-bold mb-6 text-gray-800">Ajouter un Devis</h2>
      <div className="mb-4">
        <label htmlFor="firstname" className="block text-gray-700 font-bold mb-2">Prénom</label>
        <input
          type="text"
          id="firstname"
          value={firstname}
          onChange={(e) => setFirstname(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="lastname" className="block text-gray-700 font-bold mb-2">Nom</label>
        <input
          type="text"
          id="lastname"
          value={lastname}
          onChange={(e) => setLastname(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="email" className="block text-gray-700 font-bold mb-2">Email</label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="phone" className="block text-gray-700 font-bold mb-2">Téléphone</label>
        <input
          type="text"
          id="phone"
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="subject" className="block text-gray-700 font-bold mb-2">Sujet</label>
        <input
          type="text"
          id="subject"
          value={subject}
          onChange={(e) => setSubject(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="mb-4">
        <label htmlFor="message" className="block text-gray-700 font-bold mb-2">Message</label>
        <textarea
          id="message"
          value={message}
          onChange={(e) => setMessage(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        ></textarea>
      </div>
      <div className="mb-4">
        <label htmlFor="serviceId" className="block text-gray-700 font-bold mb-2">Service ID</label>
        <input
          type="text"
          id="serviceId"
          value={serviceId}
          onChange={(e) => setServiceId(e.target.value)}
          className="w-full p-2 border border-gray-300 rounded-lg"
          required
        />
      </div>
      <div className="text-center">
        <button type="submit" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Ajouter Devis</button>
      </div>
    </form>
  );
};

export default AddQuote;
