import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import axios from "axios";

const ViewQuote = () => {
  const params = useParams();
  const [quote, setQuote] = useState(null);

  const fetchQuote = async (id) => {
    try {
      const response = await axios.get("http://localhost:3001/quotes/" + id);
      setQuote(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Il y a une erreur pour la récupération des Citations!", error);
    }
  };

  useEffect(() => {
    if (params && params.id) {
      fetchQuote(params.id);
    }
    console.log(params);
  }, [params]);

  return (
    <div className="container mx-auto mt-8">
      {quote ? (
        <div className="max-w-md mx-auto bg-white p-8 border border-gray-300 rounded-lg shadow-md">
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{quote.firstname}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{quote.lastname}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{quote.email}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{quote.phone}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{quote.subject}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{quote.message}</h2>
          <h2 className="text-2xl font-bold mb-4 text-gray-800">{quote.service_id}</h2>

        </div>
      ) : (
        <p className="text-center text-gray-500">Chargement...</p>
      )}
    </div>
  );
}

export default ViewQuote;
